<img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=header&color=DC2626&fontColor=FEF2F2&height=256&text=Terminal%20Join&desc=Join%20ANSI%20strings%20horizontally%2Fvertically%20with%20style&fontAlignY=40" />

## 💡 About

Terminal Layout let you style and join ANSI strings horizontally/vertically.

## 📥 Installation

Install the gem and add to the application's Gemfile by executing:

```sh
bundle add terminal-join
```

If bundler is not being used to manage dependencies, install the gem by executing:

```sh
gem install terminal-join
```

## ⌨️ Usage

### 📃 API

In Ruby do:

```rb
require 'terminal/join'
```

To style ANSI string:

```rb
Terminal::Join.style(`figlet Hello, World!`)
Terminal::Join.style(
  `figlet Hello, World!`,
  :bold, :italic, # Paint arguments (check out https://github.com/janlelis/paint#usage)
  halign: :bottom, # Horizontal alignment, available options are :top, :bottom and :center (default)
  valign: :right, # Vertical alignment, available options are :left, :right and :center (default)
  width: IO.console.winsize[1],
  height: 6,
  margin: [0, 2] # [horizontal_margin, vertical_margin]
  # You can write `margin: 2` to margin on all side.
)
```

Join ANSI strings horizontally or vertically:

```rb
Terminal::Join.horizontal(*%w[Foo Bar].map { `figlet #{_1}` })
# =>
#  _____            ____
# |  ___|__   ___  | __ )  __ _ _ __
# | |_ / _ \ / _ \ |  _ \ / _` | '__|
# |  _| (_) | (_) || |_) | (_| | |
# |_|  \___/ \___/ |____/ \__,_|_|

Terminal::Join.horizontal(
  *[*'A'..'D'].map { `figlet "#{_1}\n#{_1}"` },
  separator: `figlet ,`,
  align: :bottom
)
# =>
#     _        ____       ____     ____
#    / \      | __ )     / ___|   |  _ \
#   / _ \     |  _ \    | |       | | | |
#  / ___ \    | |_) |   | |___    | |_| |
# /_/   \_\   |____/     \____|   |____/
#
#     _        ____       ____     ____
#    / \      | __ )     / ___|   |  _ \
#   / _ \     |  _ \    | |       | | | |
#  / ___ \  _ | |_) | _ | |___  _ | |_| |
# /_/   \_\( )|____/ ( ) \____|( )|____/
#          |/        |/        |/

Terminal::Join.horizontal(*%w[Foo Bar].map { `figlet #{_1}` }) do |max_height|
  "  |  \n" * max_height
end
# =>
#  _____             |   ____
# |  ___|__   ___    |  | __ )  __ _ _ __
# | |_ / _ \ / _ \   |  |  _ \ / _` | '__|
# |  _| (_) | (_) |  |  | |_) | (_| | |
# |_|  \___/ \___/   |  |____/ \__,_|_|
#                    |

Terminal::Join.vertical(*[*1..3].map { `figlet #{_1}` })
# =>
#    _
#   / |
#   | |
#   | |
#   |_|
#
#  ____
# |___ \
#   __) |
#  / __/
# |_____|
#
#  _____
# |___ /
#   |_ \
#  ___) |
# |____/

Terminal::Join.vertical(
  *[*1..3].map { `figlet #{_1.to_s * _1}` },
  separator: '-' * 8,
  align: :right
)
# =>
#                _
#               / |
#               | |
#               | |
#               |_|
#
#          --------
#      ____  ____
#     |___ \|___ \
#       __) | __) |
#      / __/ / __/
#     |_____|_____|
#
#          --------
#  _______________
# |___ /___ /___ /
#   |_ \ |_ \ |_ \
#  ___) |__) |__) |
# |____/____/____/

Terminal::Join.vertical(*[*1..3].map { `figlet #{_1.to_s * _1}` }) do |max_width|
  '-' * max_width
end
# =>
#         _
#        / |
#        | |
#        | |
#        |_|
#
# -----------------
#    ____  ____
#   |___ \|___ \
#     __) | __) |
#    / __/ / __/
#   |_____|_____|
#
# -----------------
#  _______________
# |___ /___ /___ /
#   |_ \ |_ \ |_ \
#  ___) |__) |__) |
# |____/____/____/
```

### ➕ String and array extensions

```rb
require 'terminal/join/extensions'

`figlet Hello, World!`.style(:bold, margin: [4, 1])

%w[Foo Bar].map { `figlet #{_1}` }.hjoin
[*'A'..'D'].map { `figlet "#{_1}\n#{_1}"` }.hjoin(`figlet ,`, align: :bottom)
%w[Foo Bar].map { `figlet #{_1}` }.hjoin { "  |  \n" * _1 }

[*1..3].map { `figlet #{_1}` }.vjoin
[*1..3].map { `figlet #{_1.to_s * _1}` }.vjoin('-' * 8, align: :right)
[*1..3].map { `figlet #{_1.to_s * _1}` }.vjoin { '-' * _1 }
```

## 💌 Credits

- [**Gum**](https://github.com/charmbracelet/gum) by [Charm](https://github.com/charmbracelet)

<a href="https://codeberg.org/NNB"><img width="100%" src="https://capsule-render.vercel.app/api?type=waving&section=footer&color=DC2626&fontColor=FEF2F2&height=128&desc=Made%20with%20%26lt;3%20by%20NNB&descAlignY=80" /></a>
