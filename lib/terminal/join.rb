# frozen_string_literal: true

require 'paint'
require 'unicode/display_width'

require_relative 'join/version'

module Terminal
  module Join
    module_function

    def style(string, *ansi, halign: :center, valign: :center, width: 0, height: 0, margin: 0)
      margin = [margin, margin] unless margin.is_a?(Array)
      widths = Paint.unpaint(string).lines.map { Unicode::DisplayWidth.of _1.chomp }
      widths = [0] if widths.empty?
      max_width = [width, *widths].max
      pad_width = max_width - widths.max + margin[0] * 2
      pad_height = [(height - string.lines.size), 0].max + margin[1] * 2

      string
      .lines
      .map.with_index do |line, index|
        (' ' * pad_width)
        .insert(
          {
            left: margin[0],
            right: -1 - margin[0],
            center: pad_width / 2
          }[valign],
          Paint[line.chomp, *ansi] +
          Paint[' ' * (widths.max - (widths[index] || 0)), *ansi || nil]
        )
      end
      .then do
        ([' ' * max_width] * pad_height)
        .insert(
          {
            top: margin[1],
            bottom: -1 - margin[1],
            center: pad_height / 2
          }[halign],
          *_1
        )
      end
      .join("\n")
    end

    def horizontal(*strings, separator: nil, align: :center, &block)
      max_height = strings.flatten.map { _1.to_s.lines.size }.max
      separator = block.call max_height if block

      if separator.nil?
        strings.flatten
      else
        strings.flat_map { [_1, separator] }[0...-1]
      end
      .map { style _1.to_s, halign: align, height: max_height }
      .map { _1.lines.map(&:chomp) }
      .transpose.map(&:join).join("\n")
    end

    def vertical(*strings, separator: nil, align: :center, &block)
      Paint.unpaint(strings.flatten.join("\n"))
      .lines.map { Unicode::DisplayWidth.of _1 }.max => max_width
      separator = block.call max_width if block

      if separator.nil?
        strings.flatten
      else
        strings.flat_map { [_1, separator] }[0...-1]
      end
      .map { style _1.to_s, valign: align, width: max_width }
      .join("\n")
    end
  end
end
