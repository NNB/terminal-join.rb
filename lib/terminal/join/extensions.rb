# frozen_string_literal: true

require_relative '../join' unless defined? Terminal::Join

class String
  def style(*ansi, halign: :center, valign: :center, width: 0, height: 0, margin: 0)
    Terminal::Join.style(self, *ansi, halign:, valign:, width:, height:, margin:)
  end
end

class Array
  def hjoin(separator = nil, align: :center, &block)
    Terminal::Join.horizontal(*self, separator:, align:, &block)
  end

  def vjoin(separator = nil, align: :center, &block)
    Terminal::Join.vertical(*self, separator:, align:, &block)
  end
end
