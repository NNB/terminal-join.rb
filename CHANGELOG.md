## [1.0.3] - 2023-08-14

- Change `Join` from class to module.

## [1.0.2] - 2023-08-08

- Fixed `style` bug when string have ansi code after last newline.

## [1.0.1] - 2023-01-30

- Downgrade required Ruby version from `3.1.3` to `3.1.2`.

## [1.0.0] - 2023-01-29

- Initial release.
