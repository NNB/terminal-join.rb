# frozen_string_literal: true

require_relative 'lib/terminal/join/version'

Gem::Specification.new do |spec|
  spec.name = 'terminal-join'
  spec.version = Terminal::Join::VERSION
  spec.authors = ['NNB']
  spec.email = ['nnbnh@protonmail.com']

  spec.summary = 'Join ANSI strings horizontally/vertically with style.'
  spec.description = 'Terminal Layout let you style and join ANSI strings horizontally/vertically.'
  spec.homepage = 'https://codeberg.org/NNB/terminal-join.rb'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.1.2'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/src/branch/main/CHANGELOG.md"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.extra_rdoc_files = ['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.require_paths = ['lib']

  spec.add_dependency 'paint', '~> 2.3'
  spec.add_dependency 'unicode-display_width', '~> 2.4', '>= 2.4.2'
end
